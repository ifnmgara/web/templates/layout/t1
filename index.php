<!DOCTYPE html>
<html>
<head>
  <title>Chania Template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "links.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div class="row">   
  <?php include "submenu.php"; ?>
  <?php include "content.php"; ?>  
  <?php include "subcontent.php"; ?>
</div>
<?php include "footer.php"; ?>
</body>
</html>
